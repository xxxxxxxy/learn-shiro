package com.xxy.c_jdbcrealm;

import org.apache.shiro.crypto.hash.SimpleHash;

public class App2 {
    public static void main(String[] args) {
        String str = "123";

        // 以下只是创建一个SimpleHash对象，构造器中有2个参数
        // 参数一指定了加密算法：MD5
        // 参数二就是要被加密的明文
        // 参数三：盐
        // 参数四：迭代次数
        // 注意，此时并没有加密

        SimpleHash hash = new SimpleHash("MD5", str, "aa", 3);

        // 加密，得到的密文，是一个长度固定为32位的十六进制表示的数字
        String hex = hash.toHex();
        System.out.println("hex = " + hex);

        // MD5加密算法无法破解！ MD5号称散列算法
        // 凉皮 -- 加密 --》 小吃

        // 如果有人，不是靠算法破解，而是靠强制记忆
    }
}