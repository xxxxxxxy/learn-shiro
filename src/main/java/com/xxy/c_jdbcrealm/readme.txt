1.创建数据库，和表
 DROP DATABASE IF EXISTS shiro;

	CREATE DATABASE shiro;

	USE shiro;

	CREATE TABLE permissions
	(
	 pid INT PRIMARY KEY AUTO_INCREMENT,
	 pname VARCHAR(20)
	);

	CREATE TABLE roles
	(
	 rid INT PRIMARY KEY AUTO_INCREMENT,
	 rname VARCHAR(20)
	);

	CREATE TABLE users
	(
	 uid INT PRIMARY KEY AUTO_INCREMENT,
	 username VARCHAR(20),
	 PASSWORD VARCHAR(40),
	 salt VARCHAR(20)
	);

	CREATE TABLE roles_permissions
	(
	 rid INT,
	 pid INT,
	 PRIMARY KEY(rid, pid)
	);

	CREATE TABLE users_roles
	(
	 uid INT,
	 rid INT,
	 PRIMARY KEY(uid,rid)
	);

	ALTER TABLE roles_permissions
	ADD CONSTRAINT FK FOREIGN KEY(rid)
	REFERENCES roles(rid);

	ALTER TABLE roles_permissions
	ADD CONSTRAINT FK2 FOREIGN KEY(pid)
	REFERENCES permissions(pid);

	ALTER TABLE users_roles
	ADD CONSTRAINT FK3 FOREIGN KEY(uid)
	REFERENCES users(uid);

	ALTER TABLE users_roles
	ADD CONSTRAINT FK4 FOREIGN KEY(rid)
	REFERENCES roles(rid);

    INSERT INTO permissions VALUES(NULL,'user:save');
    INSERT INTO permissions VALUES(NULL,'user:delete');
    INSERT INTO permissions VALUES(NULL,'user:update');
    INSERT INTO permissions VALUES(NULL,'user:find');

    INSERT INTO roles VALUES(NULL, 'admin');
    INSERT INTO roles VALUES(NULL, 'guest');

    INSERT INTO users VALUES(NULL, 'foo','123', 'aa');
    INSERT INTO users VALUES(NULL, 'bar','456', 'bb');

    INSERT INTO roles_permissions VALUES(1,1);
    INSERT INTO roles_permissions VALUES(1,2);
    INSERT INTO roles_permissions VALUES(1,3);
    INSERT INTO roles_permissions VALUES(1,4);

    INSERT INTO roles_permissions VALUES(2,1);
    INSERT INTO roles_permissions VALUES(2,4);

    INSERT INTO users_roles VALUES(1, 1);
    INSERT INTO users_roles VALUES(2, 2);

2. 引入相关依赖
    <dependency>
        <groupId>org.apache.shiro</groupId>
        <artifactId>shiro-all</artifactId>
        <version>1.4.1</version>
    </dependency>
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid</artifactId>
        <version>1.1.16</version>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.47</version>
    </dependency>