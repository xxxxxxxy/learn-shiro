package com.xxy.a_hello;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;

public class App {
    public static void main(String[] args) {
        // 创建一个安全管理器
        DefaultSecurityManager securityManager = new DefaultSecurityManager();
        // 加载配置文件,把配置文件中的信息加载到IniRealm对象中
        // 也就是说，把硬盘上的信息，加载到内存中了
        IniRealm realm = new IniRealm("classpath:com/xxy/a_hello/shiro.ini");

        // 安全管理器就是shiro中的总管
        // 把读取到的配置信息，设置到securityManager 中
        securityManager.setRealm(realm);

        // 还要把 securityManager 设置为全局的（将来在web环境中，这一行代码不用写）
        SecurityUtils.setSecurityManager(securityManager);

        // 获取要认证的主体，就是当前要登录系统的那个东西
        // 在shiro中，就是用subject对象，代表要登录系统的实体（可能是个人，也可能是个程序）
        Subject subject = SecurityUtils.getSubject();

        // 获取主体提交来的账户密码（大部分时从表单提交来的）
        // 假设以下数据是从前端获取的
        String username = "bar";
        String password = "456";

        // 拿着客户传来账户和密码，封装成shiro中的一个令牌
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);

        System.out.println("认证前的身份" + subject.getPrincipal());
        try {
            // 让主体，拿着令牌去认证
            // 认证成功就通过，认证失败就抛出异常
            subject.login(token);
            System.out.println("认证通过");
        } catch (Exception e) {
            System.out.println("认证失败");
            e.printStackTrace();
        }

        System.out.println("是否通过认证" + subject.isAuthenticated());
        System.out.println("认证后的身份" + subject.getPrincipal());

        // 进行授权操作，记住，授权的前提是必须先通过认证
        //                                who                    what how
        System.out.println("user.save " + subject.isPermitted("user:save"));
        System.out.println("user.delete " + subject.isPermitted("user:delete"));
        System.out.println("user.update " + subject.isPermitted("user:update"));
        System.out.println("user.find " + subject.isPermitted("user:find"));

        // 主体登出
        subject.logout();
        System.out.println("用户登出");
        System.out.println("是否通过认证" + subject.isAuthenticated());
        System.out.println("user.save " + subject.isPermitted("user:save"));
        System.out.println("user.delete " + subject.isPermitted("user:delete"));
        System.out.println("user.update " + subject.isPermitted("user:update"));
        System.out.println("user.find " + subject.isPermitted("user:find"));
    }
}