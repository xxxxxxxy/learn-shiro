package com.xxy.d_cache;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.Subject;

public class App {
    public static void main(String[] args) {
        DefaultSecurityManager manager = new DefaultSecurityManager();

        // ================================================
        Realm realm = new MyRealm();
        // ===============================================
        manager.setRealm(realm);
        SecurityUtils.setSecurityManager(manager);

        EhCacheManager cacheManager = new EhCacheManager();
        // 设置缓存的配置文件
        cacheManager.setCacheManagerConfigFile("classpath:com/xxy/d_cache/ehcache.xml");
        manager.setCacheManager(cacheManager);

        Subject subject = SecurityUtils.getSubject();
        String username = "foo";
        String password = "123";
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);

        try {
            subject.login(token);
            System.out.println("认证通过");
        } catch (Exception e) {
            System.out.println("认证失败");
            e.printStackTrace();
        }

        System.out.println("认证是否通过：" + subject.isAuthenticated());
        System.out.println("身份是：" + subject.getPrincipal());
        System.out.println("user:save " + subject.isPermitted("user:save"));
        System.out.println("user:delete " + subject.isPermitted("user:delete"));
        System.out.println("user:update " + subject.isPermitted("user:update"));
        System.out.println("user:find " + subject.isPermitted("user:find"));
    }
}